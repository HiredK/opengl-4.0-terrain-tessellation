#include "Camera.h"

Camera::Camera()
{
	reset();
}

void Camera::update(unsigned t)
{
	m_positionOld=m_position;
	const float speed=(sf::Keyboard::isKeyPressed(sf::Keyboard::LShift))?15.f:5.0f;
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		m_position+=m_direction*speed;

	if(sf::Keyboard::isKeyPressed(sf::Keyboard::S)) 
		m_position-=m_direction*speed;

	if(sf::Keyboard::isKeyPressed(sf::Keyboard::D)) 
		m_position+=m_right*speed;

	if(sf::Keyboard::isKeyPressed(sf::Keyboard::A)) 
		m_position-=m_right*speed;
}

const glm::mat4 Camera::compute(sf::Window& parent, float alpha)
{
	if(sf::Mouse::isButtonPressed(sf::Mouse::Right)||!m_initialize)
	{
		if(m_initialize)
		{
			const float halfW=parent.getSize().x*0.5f;
			const float halfH=parent.getSize().y*0.5f;

			const float mX_old=m_mouse.x;
			const float mY_old=m_mouse.y;
			m_mouse.x=(sf::Mouse::getPosition(parent).x+mX_old)*0.5f;
			m_mouse.y=(sf::Mouse::getPosition(parent).y+mY_old)*0.5f;
			if(m_reset)
			{
				m_mouse.x=halfW;
				m_mouse.y=halfH;
				m_reset=false;
			}

			m_angle.x+=(halfW-m_mouse.x)*0.0018f;
			m_angle.y+=(halfH-m_mouse.y)*0.0018f;
			sf::Mouse::setPosition(sf::Vector2i(int(halfW),int(halfH)),parent);
			parent.setMouseCursorVisible(false);
		}

		m_right=glm::vec3(sin(m_angle.x-3.14f/2.0f),0,cos(m_angle.x-3.14f/2.0f));
		m_direction=glm::vec3(cos(m_angle.y)*sin(m_angle.x),sin(m_angle.y),cos(m_angle.y)*cos(m_angle.x));
	} else if(!m_reset)
	{
		parent.setMouseCursorVisible(true);
		m_reset=true;
	}

	// perform linear interpolation between two steps
	const glm::vec3 lerp=m_positionOld+alpha*(m_position-m_positionOld);

	// compute camera matrices for rendering
	const glm::mat4 pMatrix=glm::perspective(65.0f,float(parent.getSize().x/parent.getSize().y),1.0f,50000.f);
	const glm::mat4 vMatrix=glm::lookAt(lerp,lerp+m_direction,glm::cross(m_right,m_direction));
	m_initialize=true;

	return glm::mat4(pMatrix*vMatrix);
}

void Camera::reset()
{
	m_position=glm::vec3(15000*0.5, 2000, 15000*0.5);
	m_positionOld=glm::vec3(0);
	m_direction=glm::vec3(0);
	m_right=glm::vec3(0);

	m_mouse=glm::vec2(0);
	m_angle=glm::vec2(3.14f,0);

	m_focused=true;
	m_initialize=false;
	m_reset=false;
}