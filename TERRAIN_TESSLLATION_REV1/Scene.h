#ifndef SCENE_H
	#define SCENE_H

#include "common.h"
#include "Program.h"
#include "Camera.h"

class Scene {
	public:
		Scene();

		bool initialize();
		void update(unsigned int t);
		void render(sf::Window& parent, float alpha=1.f);
		bool cleanup();

	private:
		Camera m_camera;
		unsigned int m_patchVBuffer;
		unsigned int m_patchEBuffer;
		unsigned int m_patchECount;
		unsigned int m_height;
		unsigned int m_normal;
		Program m_program;
		float m_LODFactor;
};

#endif