#ifndef PROGRAM_H
	#define PROGRAM_H

#include "common.h"

class Program {
	public:
		Program();

		bool addShader(const std::string& name, unsigned int type=GL_VERTEX_SHADER);
		void build();

		void uniform(const std::string& name, int v0) const {
			glUniform1iARB(glGetUniformLocationARB(m_id,name.c_str()),v0);
		}

		void uniform(const std::string& name, float v0) const {
			glUniform1fARB(glGetUniformLocationARB(m_id,name.c_str()),v0);
		}

		void uniform(const std::string& name, float v0, float v1) const {
			glUniform2fARB(glGetUniformLocationARB(m_id,name.c_str()),v0,v1);
		}

		void uniform(const std::string& name, float v0, float v1, float v2) const {
			glUniform3fARB(glGetUniformLocationARB(m_id,name.c_str()),v0,v1,v2);
		}
		
		void uniform(const std::string& name, float v0, float v1, float v2, float v3) const {
			glUniform4fARB(glGetUniformLocationARB(m_id,name.c_str()),v0,v1,v2,v3);
		}

		void uniformMatrix3(const std::string& name, int count, bool transpose, const float* array) const {
			glUniformMatrix3fv(glGetUniformLocation(m_id,name.c_str()),count,transpose,array);
		}

		void uniformMatrix4(const std::string& name, int count, bool transpose, const float* array) const {
			glUniformMatrix4fv(glGetUniformLocation(m_id,name.c_str()),count,transpose,array);
		}

	private:
		unsigned int m_id;
};

#endif