#ifndef APPLICATION_H
	#define APPLICATION_H

#include "common.h"
#include "Scene.h"

class Application {
	public:
		Application();
		bool execute(int argc, char** argv);

	protected:
		bool onInit(int argc, char** argv);
		void onUpdate(unsigned int t);
		void onRender(float alpha=1.f);
		bool onCleanup();

	private:
		sf::Window m_window;
		Scene m_scene;
};

#endif