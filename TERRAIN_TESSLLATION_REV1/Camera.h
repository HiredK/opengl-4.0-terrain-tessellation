#ifndef CAMERA_H
	#define CAMERA_H

#include "common.h"

class Camera {
	public:
		Camera();

		void update(unsigned t);
		const glm::mat4 compute(sf::Window& parent, float alpha=1.f);
		const glm::vec3& getPosition() { return m_position; }
		void reset();

	private:
		glm::vec3 m_position;
		glm::vec3 m_positionOld;
		glm::vec3 m_direction;
		glm::vec3 m_right;

		glm::vec2 m_mouse;
		glm::vec2 m_angle;

		bool m_focused;
		bool m_initialize;
		bool m_reset;
};

#endif