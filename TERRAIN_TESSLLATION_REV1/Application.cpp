#include "Application.h"

Application::Application()
{
}

bool Application::execute(int argc, char** argv)
{
	if(onInit(argc,argv))
	{
		sf::Clock clock[2];
		unsigned int t=0;
		unsigned int f=0;
		while(m_window.isOpen())
		{
			if(clock[0].getElapsedTime().asSeconds()>APPLICATION_TIMESTEP)
			{
				onUpdate(t++);
				clock[0].restart();
			}

			f++;
			onRender(clock[0].getElapsedTime().asSeconds()/APPLICATION_TIMESTEP);
			if(clock[1].getElapsedTime().asMilliseconds()>1000)
			{
				std::stringstream ss;
				const int FPS=int(1000/(clock[1].restart().asMilliseconds()/float(f)));
				ss<<APPLICATION_CAPTION<<" ["<<FPS<<" FPS]";
				m_window.setTitle(ss.str());
				f=0;
			}
		}

		return onCleanup();
	} return false;
}

bool Application::onInit(int argc, char** argv)
{
	if(PHYSFS_init(argv[0]))
	{
		const std::string root="data/";
		PHYSFS_addToSearchPath((PHYSFS_getBaseDir()+root).c_str(),1);
		PHYSFS_setWriteDir(PHYSFS_getBaseDir());
		PHYSFS_mkdir(root.c_str());

		printf("/* %s */\n\n",APPLICATION_CAPTION);
		//m_window.create(sf::VideoMode(1366,768),APPLICATION_CAPTION,sf::Style::Fullscreen,sf::ContextSettings(32,8,16));
		m_window.create(sf::VideoMode(1024,600),APPLICATION_CAPTION,sf::Style::Close);//,sf::ContextSettings(32,8,16));
		//m_window.setVerticalSyncEnabled(true);

		unsigned int error;
		if((error=glewInit())!=GLEW_OK)
		{
			printf("Error: %s\n",glewGetErrorString(error));
			sf::sleep(sf::seconds(1.f));
			return false;
		}

		if(!glewIsSupported("GL_VERSION_4_0"))
		{
			printf("Error: OpenGL 4.0 is required\n");
			sf::sleep(sf::seconds(1.f));
			return false;
		}

		printf("Info: OpenGL %s on %s %s\n",glGetString(GL_VERSION),glGetString(GL_VENDOR),glGetString(GL_RENDERER));
		printf("Info: GLSL version supported %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));
		printf("Successfully initialized GLEW %s context\n\n",glewGetString(GLEW_VERSION));
		return m_scene.initialize();
	}

	return false;
}

void Application::onUpdate(unsigned int t)
{
	sf::Event event;
	while(m_window.pollEvent(event))
	{
		switch(event.type)
		{
			case sf::Event::Closed:
				m_window.close();
				return;
		}
	}

	m_scene.update(t);
}

void Application::onRender(float alpha)
{
	if(m_window.isOpen())
	{
		glClearColor(1,1,1,1);
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
		m_scene.render(m_window,alpha);
		m_window.display();
	}
}

bool Application::onCleanup()
{
	if(!PHYSFS_deinit()||!m_scene.cleanup())
	{
		printf("Error: Unable to shutdown properly\n");
		sf::sleep(sf::seconds(1.f));
		return false;
	}

	printf("Exiting...\n");
	return true;
}