#ifndef COMMON_H
	#define COMMON_H

#define APPLICATION_CAPTION "Terrain Tessllation REV1"
#define APPLICATION_TIMESTEP 0.01f // 1.f/sec

#define TERRAIN_PATCHES_NUM_X 128
#define TERRAIN_PATCHES_NUM_Z 128
#define TERRAIN_DEFAULT_LOD 12

// STD include
#include <sstream>

// SFML include
#define SFML_STATIC
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

// GLEW include
#define GLEW_STATIC
#include <GL/glew.h>

// GLM include
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// PHYSFS include
#include <physfs.h>

#endif