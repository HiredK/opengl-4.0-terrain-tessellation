#include "Program.h"

Program::Program(): m_id(0)
{
}

bool Program::addShader(const std::string& name, unsigned int type)
{
	if(PHYSFS_exists(name.c_str()))
	{
		PHYSFS_file* FILE=PHYSFS_openRead(name.c_str());
		size_t size=(size_t)PHYSFS_fileLength(FILE);
		char* data=new char[size+1];
		PHYSFS_read(FILE,data,1,size);
		data[size]='\0';
		
		unsigned int shd=glCreateShader(type);
		glShaderSource(shd,1,(const char**)&data,NULL);
		glCompileShader(shd);

		int status;
		glGetShaderiv(shd,GL_COMPILE_STATUS,&status);
		if(status==GL_FALSE)
		{
			int lenght;
			glGetShaderiv(shd,GL_INFO_LOG_LENGTH,&lenght);

			char* log=new char[lenght+1];
			glGetShaderInfoLog(shd,lenght,NULL,log);
			printf("Error: %s\n",log);
			delete[] log;
		}
		else
		{
			if(m_id==0)
			{
				m_id=glCreateProgram();
			}

			printf("Attaching '%s' to program %d\n",name.c_str(),m_id);
			glAttachShader(m_id,shd);
		} PHYSFS_close(FILE);

		delete data;
		data=NULL;
		return true;
	}

	return false;
}

void Program::build()
{
	if(m_id)
	{
		printf("Linking program %d\n",m_id);
		glLinkProgram(m_id);
		glUseProgram(m_id);
	}
}