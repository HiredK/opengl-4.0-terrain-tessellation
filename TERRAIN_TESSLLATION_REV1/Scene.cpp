#include "Scene.h"

Scene::Scene(): m_LODFactor(TERRAIN_DEFAULT_LOD)
{
	m_patchVBuffer=0;
	m_patchEBuffer=0;
	m_patchECount=0;
	m_height=0;
	m_normal=0;
}

bool Scene::initialize()
{
	{
		printf("1) BUILDING TERRAIN PROGRAM\n");

		if(!m_program.addShader("terrain.vs",GL_VERTEX_SHADER))
			printf("Error: Unable to find 'terrain.vs'\n");

		if(!m_program.addShader("terrain.tcs",GL_TESS_CONTROL_SHADER))
			printf("Error: Unable to find 'terrain.tcs'\n");

		if(!m_program.addShader("terrain.tes",GL_TESS_EVALUATION_SHADER))
			printf("Error: Unable to find 'terrain.tes'\n");

		if(!m_program.addShader("terrain.fs",GL_FRAGMENT_SHADER))
			printf("Error: Unable to find 'terrain.fs'\n");

		m_program.build();
		printf("DONE\n\n");
	}

	if(PHYSFS_exists("terrain.raw"))
	{
		printf("2) BUILDING TERRAIN HEIGHT TEXTURE\n");
	
		PHYSFS_file* FILE=PHYSFS_openRead("terrain.raw");
		size_t size=(size_t)PHYSFS_fileLength(FILE);
		char* data=new char[size+1];

		PHYSFS_read(FILE,data,1,size);
		PHYSFS_close(FILE);
		data[size]='\0';
			
		glGenTextures(1,&m_height);
		glBindTexture(GL_TEXTURE_2D,m_height);
		glTexImage2D(GL_TEXTURE_2D,0,GL_LUMINANCE32F_ARB,4096,4096,0,GL_LUMINANCE,GL_FLOAT,data);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		delete[] data;

		printf("DONE\n\n");
	}

	if(PHYSFS_exists("terrain_n.png"))  
	{
		printf("3) BUILDING TERRAIN NORMAL TEXTURE\n");

		PHYSFS_file* FILE=PHYSFS_openRead("terrain_n.png");
		size_t size=(size_t)PHYSFS_fileLength(FILE);
		char* data=new char[size+1];

		PHYSFS_read(FILE,data,1,size);
		PHYSFS_close(FILE);
		data[size]='\0';

		sf::Image image;
		if(image.loadFromMemory(data,size))
		{
			glGenTextures(1,&m_normal);
			glBindTexture(GL_TEXTURE_2D,m_normal);
			glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,image.getSize().x,image.getSize().y,0,GL_RGBA,GL_UNSIGNED_BYTE,image.getPixelsPtr());
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		} delete[] data;

		printf("DONE\n\n");
	}

	{
		printf("4) BUILDING TERRAIN PATCHES VBO\n");

		std::vector<glm::vec4> vertices;
		vertices.resize(TERRAIN_PATCHES_NUM_X*TERRAIN_PATCHES_NUM_Z);
		const float offsetX=1.f/TERRAIN_PATCHES_NUM_X;
		const float offsetZ=1.f/TERRAIN_PATCHES_NUM_Z;
		for(int z=0; z<TERRAIN_PATCHES_NUM_Z; z++)
		{
			for(int x=0; x<TERRAIN_PATCHES_NUM_X; x++)
			{
				vertices[x+z*TERRAIN_PATCHES_NUM_X]=glm::vec4(x*offsetX,z*offsetZ,1,1);
			}
		}

		std::vector<unsigned short> indices;
		indices.resize((TERRAIN_PATCHES_NUM_X-1)*(TERRAIN_PATCHES_NUM_Z-1)*4);
		for(int z=0; z<TERRAIN_PATCHES_NUM_Z-1; z++)
		{
			for(int x=0; x<TERRAIN_PATCHES_NUM_X-1; x++)
			{
				int offset=(x+z*(TERRAIN_PATCHES_NUM_X-1))*4;
				unsigned short p1=x+z*TERRAIN_PATCHES_NUM_X;
				unsigned short p2=p1+TERRAIN_PATCHES_NUM_X;
				unsigned short p3=p2+1;
				unsigned short p4=p1+1;

				indices[offset+0]=p1;
				indices[offset+1]=p2;
				indices[offset+2]=p3;
				indices[offset+3]=p4;
			}
		}

		printf("Generating VBO with %d vertices and %d indices\n",vertices.size(),indices.size());

		glGenBuffers(1,&m_patchVBuffer);
		glBindBuffer(GL_ARRAY_BUFFER,m_patchVBuffer);
		glBufferData(GL_ARRAY_BUFFER,sizeof(glm::vec4)*vertices.size(),&vertices[0],GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER,0);

		glGenBuffers(1,&m_patchEBuffer);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,m_patchEBuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(unsigned short)*indices.size(),&indices[0],GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
		m_patchECount=indices.size();
		printf("DONE\n\n");
	}

	return true;
}

void Scene::update(unsigned int t)
{
	m_camera.update(t);
}

void Scene::render(sf::Window& parent, float alpha)
{
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up)&&m_LODFactor<64.f)
		m_LODFactor+=0.25f;

	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down)&&m_LODFactor>1.f)
		m_LODFactor-=0.25f;

	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
		glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
	else
		glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);

	m_program.uniformMatrix4("World",1,false,glm::value_ptr(glm::scale(glm::mat4(1),glm::vec3(15000.f,0.5f,15000.f))));
	m_program.uniformMatrix4("View",1,false,glm::value_ptr(m_camera.compute(parent,alpha)));

	m_program.uniform("EyePos",m_camera.getPosition().x,m_camera.getPosition().y,m_camera.getPosition().z);
	m_program.uniform("LODFactor",m_LODFactor);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,m_height); // m_height
	m_program.uniform("Height",0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D,m_normal); // m_normal
	m_program.uniform("Normal",1);

	glEnableClientState(GL_VERTEX_ARRAY);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	glBindBuffer(GL_ARRAY_BUFFER,m_patchVBuffer);
	glVertexPointer(4,GL_FLOAT,sizeof(glm::vec4),(void*)0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,m_patchEBuffer);

	glPatchParameteri(GL_PATCH_VERTICES,4);
	glDrawElements(GL_PATCHES,m_patchECount,GL_UNSIGNED_SHORT,0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
	glBindBuffer(GL_ARRAY_BUFFER,0);

	glDisableClientState(GL_VERTEX_ARRAY);
}

bool Scene::cleanup()
{
	if(m_patchVBuffer)
	{
		glDeleteBuffers(1,&m_patchVBuffer);
		m_patchVBuffer=0;
	}

	if(m_patchEBuffer)
	{
		glDeleteBuffers(1,&m_patchEBuffer);
		m_patchEBuffer=0;
		m_patchECount=0;
	}

	if(m_height)
	{
		glDeleteTextures(1,&m_height);
		m_height=0;
	}

	if(m_normal)
	{
		glDeleteTextures(1,&m_normal);
		m_normal=0;
	}

	return true;
}